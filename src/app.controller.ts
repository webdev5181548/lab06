import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getDefault(): string {
    return 'Default';
  }

  @Put('hello')
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('world')
  getWorld(): string {
    return this.appService.getWorld();
  }

  @Delete('help')
  getHelp(): string {
    return '<html><body><h1>help</h1></body></html>';
  }

  @Patch('test')
  getTest(): string {
    return '<html><body><h1>test</h1></body></html>';
  }

  @Get('test-query')
  testQuery(
    @Req() req,
    @Query('celsius') celsius: number,
    @Query('type') type: string,
  ) {
    return {
      celsius: celsius,
      type: type,
    };
  }

  @Get('test-params/:celsius')
  testParams(@Req() req, @Param('celsius') celsius: number) {
    return {
      celsius,
    };
  }

  @Post('test-body')
  testBody(@Req() req, @Body() body, @Body('celsius') celsius: number) {
    return { celsius };
  }
}
